import React from "react";
import {Platform, StyleSheet, Text, View,Image, TextInput, Alert, Button} from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ecf0f1',
    },
    input: {
      width: 200,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 10,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
      },
      welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
      },
});
  const Header = ()=> {
    return (
        <View>
        <View style={styles.welcomeContainer}>
  <Image
    source={ require('../asset/images/AppIcon.png')}
    style={styles.welcomeImage}
  />
</View>
</View>
    );
  }
  

export default Header;