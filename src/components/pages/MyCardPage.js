import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image, TextInput, Alert, Button} from 'react-native';
import { Link } from "react-router-native";
import Arya from '../header'

export default class MyCardPage extends Component {
    render(){
        return (
            <View>
                <Arya/>
                <Text h1>MyCardPage</Text>
                <Link to={"/NetworkPage/"}><Text>NetworkPage</Text></Link>
            </View>
            
        )
    }
}