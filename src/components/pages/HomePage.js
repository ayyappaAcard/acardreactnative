import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image, TextInput, Alert, Button} from 'react-native';
import { Link } from "react-router-native";
import Arya from '../header'

export default class HomePage extends Component {
    render(){
        return (
            <View>
                <Arya/>
                <Text h1>HomePage</Text>
                <Link to={"/MyCardPage/"}><Text>MyCardPage</Text></Link>
            </View>
            
        )
    }
}