import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image, TextInput, Alert, Button} from 'react-native';
import { Link } from "react-router-native";
import {logo} from '../../asset/images/AppIcon.png';
import Arya from '../header'

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ecf0f1',
    },
    input: {
      width: 200,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 10,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
      },
      welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
      },
});


export default class LoginPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
        };
    }
    
    onLogin() {
        const { username, password } = this.state;
    
        Alert.alert('Credentials', `${username} + ${password}`);
    }
    
    render(){
        return(
            <View>
                {/* <View style={styles.welcomeContainer}>
          <Image
            source={
              
                require('../../asset/images/AppIcon.png')
                
            }
            style={styles.welcomeImage}
          />
        </View> */}
        <Arya/>
                <TextInput
                    value={this.state.username}
                    onChangeText={(username) => this.setState({ username })}
                    placeholder={'Username'}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.password}
                    onChangeText={(password) => this.setState({ password })}
                    placeholder={'Password'}
                    secureTextEntry={true}
                    style={styles.input}
                />
                    
                {/* <Button
                    title={'Login'}
                    style={styles.input}
                    onPress={this.onLogin.bind(this)}
                ><Link to={"/signup/"}></Link></Button> */}
                <Link to={"/MenuPage/"}><Text>Login</Text></Link>
                <Link to={"/SignUpPage/"}><Text>SignUp</Text></Link>
                
            </View>
        )
    }
}