/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import LoginPage from './src/components/pages/LoginPage';
import MenuPage from './src/components/pages/MenuPage'
import HomePage from './src/components/pages/HomePage'
import MyCardPage from './src/components/pages/MyCardPage';
import NetworkPage from './src/components/pages/NetworkPage';
import SettingPage from './src/components/pages/SettingPage';
import ShareCardPage from './src/components/pages/ShareCardPage';
import SignUpPage from './src/components/pages/SignUpPage';
import QRPage from './src/components/pages/QRPage';

import { Route, Switch, NativeRouter as Router } from "react-router-native";



export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Router>
          <Switch>
            <Route
              exact
              path="/"
              render={props => (
                <LoginPage/>
              )}
            />
            <Route
              path="/MenuPage/"
              render={props => (
                <MenuPage
                />
              )}
            />
            <Route
              path="/HomePage/"
              render={props => (
                <HomePage
                />
              )}
            />
             <Route
              path="/MyCardPage/"
              render={props => (
                <MyCardPage
                />
              )}
            />
            <Route
              path="/NetworkPage/"
              render={props => (
                <NetworkPage
                />
              )}
            />
              <Route
              path="/QRPage/"
              render={props => (
                <QRPage
                />
              )}
            />
            <Route
              path="/SettingPage/"
              render={props => (
                <SettingPage
                />
              )}
            />
             <Route
              path="/ShareCardPage/"
              render={props => (
                <ShareCardPage
                />
              )}
            />
             <Route
              path="/SignUpPage/"
              render={props => (
                <SignUpPage
                />
              )}
            />
            
          </Switch>
        </Router>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
